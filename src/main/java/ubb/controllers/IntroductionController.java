package ubb.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import ubb.domain.*;
import ubb.service.AngajatService;
import ubb.service.PontajService;
import ubb.service.SarcinaService;

import java.util.*;
import java.util.stream.Collectors;

public class IntroductionController {

    AngajatService angajatService;
    SarcinaService sarcinaService;
    PontajService pontajService;
    Stage introductionStage;

    ObservableList<Angajat> modelAngajati = FXCollections.observableArrayList();
    ObservableList<AngajatDTO> modelAngajatiDTO = FXCollections.observableArrayList();

    @FXML
    TableView<Angajat> tableViewAngajati;

    @FXML
    TableColumn<Angajat, String> tableColumnNume;

    @FXML
    TableColumn<Angajat, Float> tableColumnVenit;

    @FXML
    TableColumn<Angajat, Nivel> tableColumnNivel;

    @FXML
    TableView<AngajatDTO> tableViewAngajatiDTO;

    @FXML
    TableColumn<AngajatDTO, String> tableColumnNumeDTO;

    @FXML
    TableColumn<AngajatDTO, Nivel> tableColumnNivelDTO;

    @FXML
    TableColumn<AngajatDTO, Float> tableColumnSalarDTO;


    @FXML
    Button buttonFiltrare;

    @FXML
    Button buttonRevin;

    @FXML
    Button buttonAfisareSarcini;

    @FXML
    TextField textFieldUsoara;

    @FXML
    TextField textFieldMedie;

    @FXML
    TextField textFieldGrea;

    @FXML
    ComboBox comboBoxLuna;


    @FXML
    public void initialize(){
        tableColumnNume.setCellValueFactory(new PropertyValueFactory<Angajat, String>("Nume"));
        tableColumnVenit.setCellValueFactory(new PropertyValueFactory<Angajat, Float>("VenitPeOra"));
        tableColumnNivel.setCellValueFactory(new PropertyValueFactory<Angajat, Nivel>("Nivel"));
        tableViewAngajati.setItems(modelAngajati);

        tableColumnNumeDTO.setCellValueFactory(new PropertyValueFactory<AngajatDTO, String>("Nume"));
        tableColumnSalarDTO.setCellValueFactory(new PropertyValueFactory<AngajatDTO, Float>("SalarLuna"));
        tableColumnNivelDTO.setCellValueFactory(new PropertyValueFactory<AngajatDTO, Nivel>("Nivel"));
        tableViewAngajatiDTO.setItems(modelAngajatiDTO);
    }

    public void initModel(){
        modelAngajati.setAll((Collection<? extends Angajat>) angajatService.getAll());
    }

    public void setAngajatiService(AngajatService angajatService) {
        this.angajatService = angajatService;
    }

    public void setSarciniService(SarcinaService sarcinaService) {
        this.sarcinaService = sarcinaService;
    }

    public void setPontajeService(PontajService pontajService) {
        this.pontajService = pontajService;
        initModel();
    }

    public void setIntroductionstage(Stage primaryStage) {
        this.introductionStage = introductionStage;
    }

    public void filtrareAngajati() {
       Iterable<Angajat> list = angajatService.getAll();
       List<Angajat> listaAngajati = new ArrayList<>();

       list.forEach(a->listaAngajati.add(a));

       // listaAngajati.forEach(System.out::println);

        List<Angajat> ordered =
                listaAngajati.stream()
                .sorted(Comparator.comparing(Angajat::getNivel)
                        .thenComparing(Comparator.comparing(Angajat::getVenitPeOra).reversed()))
                .collect(Collectors.toList());

        //ordered.forEach(System.out::println);

        modelAngajati.setAll(ordered);
    }

    public void afisareAngajati() {
        initModel();
    }

    public void afisareSarcini() {

/*        List<Sarcina> sarciniUsoare = sarcinaService.getSarcinaDificultate(Dificultate.Usoara);
        List<Sarcina> sarciniMedii = sarcinaService.getSarcinaDificultate(Dificultate.Medie);
        List<Sarcina> sarciniGrele = sarcinaService.getSarcinaDificultate(Dificultate.Grea);

        final int[] sumUsoare = {0};
        sarciniUsoare.forEach(s-> {
            sumUsoare[0] = sumUsoare[0] +  s.getNrOreEstimate();
        });

        Double medieUsoare = Double.valueOf(sumUsoare[0] / sarciniUsoare.size());

        textFieldUsoara.setText(medieUsoare.toString());*/
    //---------------------------------------------------------
        Iterable<Sarcina> iterable = sarcinaService.getAll();

        List<Sarcina> sarcini = new ArrayList<>();
        iterable.forEach(s->sarcini.add(s));

        Map<Dificultate,List<Sarcina>> map=sarcini.stream()
                .collect(Collectors.groupingBy(Sarcina::getDificultate)); //grupez dupa dificultate

        map.forEach((key, value) -> {
            double media = value
                    .stream()
                    .mapToDouble(Sarcina::getNrOreEstimate)
                    .average()
                    .orElse(0);

            //System.out.println(media);

            if (key.equals(Dificultate.Usoara))
                textFieldUsoara.setText(String.valueOf(media));

            if (key.equals(Dificultate.Medie))
                textFieldMedie.setText(String.valueOf(media));

            if (key.equals(Dificultate.Grea))
                textFieldGrea.setText(String.valueOf(media));

        });

        //map.entrySet().forEach(System.out::println);

    }

    public void afisareHarnici() {
        //Sa se afiseze primii 2 cei mai harnici angajați (nr de ore lucrate X venit pe ora ->maxim)
        modelAngajatiDTO.setAll(pontajService.harnici());
    }

    public void selectComboBox() {
        comboBoxLuna.getItems().setAll("01", "02", "03", "04", "05", "06", "07",
                "08", "09", "10", "11","12");
    }

    public void raport() {
        int luna =  Integer.parseInt((String) comboBoxLuna.getSelectionModel().getSelectedItem());
       // System.out.println(luna);
        modelAngajatiDTO.setAll(pontajService.raport(luna));
    }
}
