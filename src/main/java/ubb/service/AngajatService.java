package ubb.service;


import ubb.domain.Angajat;
import ubb.repository.Repository;


import java.util.ConcurrentModificationException;

public class AngajatService  {
    private final Repository<String, Angajat> repoAngajat;
    
    public AngajatService(Repository<String, Angajat> repoAngajat) {
        this.repoAngajat = repoAngajat;

    }

    /**
     *saves the user received parameter
     * @param AngajatParam
     * @return the user created
     */
    public Angajat addAngajat(Angajat AngajatParam) {
        Angajat angajat = repoAngajat.save(AngajatParam);

        return angajat;
    }

    ///TO DO: add other methods

    /**
     *delete the user identified by the received id, and also delete that user's friendships
     * @param id long - id's to be deleted
     * @return deleted user
     * @throws ConcurrentModificationException
     */
    public Angajat deleteAngajat(String id) throws ConcurrentModificationException {
        Angajat angajat = repoAngajat.delete(id);

        return angajat;
    }

    /**
     *
     * @return an iterable list of all saved users
     */
    public Iterable<Angajat> getAll(){
        return repoAngajat.findAll();
    }
    public Angajat findOne(String id){
        return repoAngajat.findOne(id);
    }


}
