package ubb.service;

import ubb.domain.*;
import ubb.domain.Pontaj;
import ubb.repository.Repository;

import java.util.*;
import java.util.stream.Collectors;

public class PontajService {
    private Repository<Tuple<String,String>, Pontaj> repoPontaj;
    private Repository<String, Angajat> repoAngajat;
    private Repository<String, Sarcina> repoSarcina;
    private List<AngajatDTO> angajatiDTO = new ArrayList<>();

    public PontajService(Repository<Tuple<String, String>, Pontaj> repoPontaj, Repository<String, Angajat> repoAngajat, Repository<String, Sarcina> repoSarcina) {
        this.repoPontaj = repoPontaj;
        this.repoAngajat = repoAngajat;
        this.repoSarcina = repoSarcina;
    }

    /**
     *saves the user received parameter
     * @param PontajParam
     * @return the user created
     */
    public Pontaj addPontaj(Pontaj PontajParam) {
        Pontaj pontaj = repoPontaj.save(PontajParam);

        return pontaj;
    }

    ///TO DO: add other methods

    /**
     *delete the user identified by the received id, and also delete that user's friendships
     * @param id long - id's to be deleted
     * @return deleted user
     * @throws ConcurrentModificationException
     */
    public Pontaj deletePontaj(Tuple<String,String> id) throws ConcurrentModificationException {
        Pontaj Pontaj = repoPontaj.delete(id);

        return Pontaj;
    }

    /**
     *
     * @return an iterable list of all saved users
     */
    public Iterable<Pontaj> getAll(){
        return repoPontaj.findAll();
    }
    public Pontaj findOne(String idLeft, String idRight){
        return repoPontaj.findOne(new Tuple<>(idLeft,idRight));
    }

/*    public List<Angajat> harnici(){
        Iterable<Pontaj> iterable = repoPontaj.findAll();
        List<Pontaj> pontaje = new ArrayList<>();
        iterable.forEach(pontaje::add);

*//*        Map<Dificultate,List<Sarcina>> map=sarcini.stream()
                .collect(Collectors.groupingBy(x->x.getDificultate()));*//*

        Map<String,List<Pontaj>> map = pontaje.stream()
                .collect(Collectors.groupingBy(x->x.getId().getLeft())); //grupez dupa angajati

        map.entrySet().forEach(x->{
            System.out.println(x.getValue());
        });

        return null;
    }*/

    public List<AngajatDTO> harnici(){
        Iterable<Angajat> iterable = repoAngajat.findAll();
        List<Angajat> angajati = new ArrayList<>();
        iterable.forEach(angajati::add);

        Iterable<Pontaj> iterable2 = repoPontaj.findAll();
        List<Pontaj> pontaje = new ArrayList<>();
        iterable2.forEach(pontaje::add);

        List<AngajatDTO> angajatiDTO = new ArrayList<>();

        for(Angajat a : angajati){
            float nrOre = 0;
            for(Pontaj p : pontaje){
                if(p.getId().getLeft().equals(a.getId())){
                    Sarcina sarcina = repoSarcina.findOne(p.getId().getRight());
                    nrOre += sarcina.getNrOreEstimate();
                }
            }
            float salar = nrOre * a.getVenitPeOra();
            AngajatDTO angajatDTO = new AngajatDTO(a.getNume(),a.getNivel(),salar);
            angajatiDTO.add(angajatDTO);

        }

        List<AngajatDTO> sorted =
                angajatiDTO.stream()
                .sorted(Comparator.comparing(AngajatDTO::getSalarLuna).reversed())
                        .collect(Collectors.toList());

        System.out.println(sorted.stream().limit(2).collect(Collectors.toList()));
        return sorted.stream().limit(2).collect(Collectors.toList());

    }

    public List<AngajatDTO> raport(int Luna){
        Iterable<Angajat> iterable = repoAngajat.findAll();
        List<Angajat> angajati = new ArrayList<>();
        iterable.forEach(angajati::add);

        Iterable<Pontaj> iterable2 = repoPontaj.findAll();
        List<Pontaj> pontaje = new ArrayList<>();
        iterable2.forEach(pontaje::add);

        List<AngajatDTO> angajatiDTO = new ArrayList<>();

        for(Angajat a : angajati){
            float nrOre = 0;
            for(Pontaj p : pontaje){
                if(p.getId().getLeft().equals(a.getId()) && p.getData().getMonthValue() == Luna) {
                    Sarcina sarcina = repoSarcina.findOne(p.getId().getRight());
                    nrOre += sarcina.getNrOreEstimate();
                }
            }
            float salar = nrOre * a.getVenitPeOra();
            AngajatDTO angajatDTO = new AngajatDTO(a.getNume(),a.getNivel(),salar);
            angajatiDTO.add(angajatDTO);

        }

        List<AngajatDTO> ordered =
                angajatiDTO.stream()
                        .sorted(Comparator.comparing(AngajatDTO::getNivel)
                                .thenComparing(Comparator.comparing(AngajatDTO::getSalarLuna)))
                        .collect(Collectors.toList());

       // System.out.println(angajatiDTO);

        return ordered;
    }
}
