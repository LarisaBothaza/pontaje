package ubb.service;

import ubb.domain.Dificultate;
import ubb.domain.Sarcina;
import ubb.repository.Repository;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.stream.Collectors;

public class SarcinaService {
    private final Repository<String, Sarcina> repoSarcina;

    public SarcinaService(Repository<String, Sarcina> repoSarcina) {
        this.repoSarcina = repoSarcina;

    }

    /**
     *saves the user received parameter
     * @param SarcinaParam
     * @return the user created
     */
    public Sarcina addSarcina(Sarcina SarcinaParam) {
        Sarcina sarcina = repoSarcina.save(SarcinaParam);

        return sarcina;
    }

    ///TO DO: add other methods

    /**
     *delete the user identified by the received id, and also delete that user's friendships
     * @param id long - id's to be deleted
     * @return deleted user
     * @throws ConcurrentModificationException
     */
    public Sarcina deleteSarcina(String id) throws ConcurrentModificationException {
        Sarcina Sarcina = repoSarcina.delete(id);

        return Sarcina;
    }

    /**
     *
     * @return an iterable list of all saved users
     */
    public Iterable<Sarcina> getAll(){
        return repoSarcina.findAll();
    }

    public List<Sarcina> getSarcinaDificultate(Dificultate dificultate){
       Iterable<Sarcina> iterable = repoSarcina.findAll();
        List<Sarcina> sarcini = new ArrayList<>();

        iterable.forEach(s->sarcini.add(s));

        return sarcini.stream()
                .filter(fr->fr.getDificultate().equals(dificultate))
                .collect(Collectors.toList());


    }

    public Sarcina findOne(String id){
        return repoSarcina.findOne(id);
    }


}
