package ubb.repository.file;

import ubb.domain.Angajat;
import ubb.domain.Nivel;

import java.util.List;

public class AngajatFile extends AbstractFileRepository<String, Angajat> {
    public AngajatFile(String fileName) {
        super(fileName);
    }

    @Override
    public Angajat extractEntity(List<String> attributes) {
        Angajat angajat= new Angajat(attributes.get(1),Float.parseFloat(attributes.get(2)), Nivel.valueOf(attributes.get(3)));
        angajat.setId(attributes.get(0));

        return angajat;
    }

    @Override
    protected String createEntityAsString(Angajat entity) {
        return entity.getId()+","+entity.getNume()+","+entity.getVenitPeOra()+","+entity.getNivel();
    }
}
