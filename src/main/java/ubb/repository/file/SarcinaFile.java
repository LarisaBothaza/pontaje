package ubb.repository.file;

import ubb.domain.Angajat;
import ubb.domain.Dificultate;
import ubb.domain.Nivel;
import ubb.domain.Sarcina;

import java.util.List;

public class SarcinaFile extends AbstractFileRepository<String, Sarcina> {
    public SarcinaFile(String fileName) {
        super(fileName);
    }

    @Override
    public Sarcina extractEntity(List<String> attributes) {
        Sarcina sarcina= new Sarcina(Dificultate.valueOf(attributes.get(1)), Integer.parseInt(attributes.get(2)));
        sarcina.setId(attributes.get(0));

        return sarcina;
    }

    @Override
    protected String createEntityAsString(Sarcina entity) {
        return entity.getId()+","+entity.getDificultate()+","+entity.getNrOreEstimate();
    }
}
