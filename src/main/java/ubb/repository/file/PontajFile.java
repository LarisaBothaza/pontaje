package ubb.repository.file;

import ubb.domain.Angajat;
import ubb.domain.Nivel;
import ubb.domain.Pontaj;
import ubb.domain.Tuple;
import ubb.utils.Constants;

import java.time.LocalDateTime;
import java.util.List;

public class PontajFile extends AbstractFileRepository<Tuple<String,String>, Pontaj> {

    public PontajFile(String fileName) {
        super(fileName);
    }

    @Override
    public Pontaj extractEntity(List<String> attributes) {

        Pontaj pontaj= new Pontaj(LocalDateTime.parse(attributes.get(2),Constants.DATE_TIME_FORMATER));
        pontaj.setId(new Tuple<>(attributes.get(0),attributes.get(1)));

        return pontaj;
    }

    @Override
    protected String createEntityAsString(Pontaj entity) {
        return entity.getId().getLeft()+","+entity.getId().getRight()+","+entity.getData().format(Constants.DATE_TIME_FORMATER);
    }


}
