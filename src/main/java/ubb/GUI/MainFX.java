package ubb.GUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import ubb.config.ApplicationContext;
import ubb.controllers.IntroductionController;
import ubb.domain.Angajat;
import ubb.domain.Pontaj;
import ubb.domain.Sarcina;
import ubb.domain.Tuple;
import ubb.repository.Repository;
import ubb.repository.file.AngajatFile;
import ubb.repository.file.PontajFile;
import ubb.repository.file.SarcinaFile;
import ubb.service.AngajatService;
import ubb.service.PontajService;
import ubb.service.SarcinaService;

import java.io.IOException;


public class MainFX extends Application {

    private static AngajatService angajatService;
    private static SarcinaService sarcinaService;
    private static PontajService pontajService;

    @Override
    public void start(Stage primaryStage) throws Exception {
//        Group root = new Group();
//        Scene scene = new Scene(root,500,500, Color.PINK);
//        primaryStage.setTitle("Test");
//        primaryStage.setScene(scene);
        initView(primaryStage);
        //prefHeight="400.0" prefWidth="250.0"
        //primaryStage.setWidth(290);
        //primaryStage.setHeight(450);

        primaryStage.setTitle("Welcome!");
        primaryStage.show();


    }

    public static void main(String[] args) {
        //configurations
        String fileNameAngajati=ApplicationContext.getPROPERTIES().getProperty("data.ubb.angajati");
        String fileNamePontaje=ApplicationContext.getPROPERTIES().getProperty("data.ubb.pontaje");
        String fileNameSarcini=ApplicationContext.getPROPERTIES().getProperty("data.ubb.sarcini");
        //String fileName="data/users.csv";

        //repositories
        Repository<String, Angajat> angajatFileRepository = new AngajatFile(fileNameAngajati);
        Repository<String, Sarcina> sarcinaFileRepository = new SarcinaFile(fileNameSarcini);
        Repository<Tuple<String,String>, Pontaj> pontajFileRepository = new PontajFile(fileNamePontaje);


        //services

        angajatService = new AngajatService(angajatFileRepository);
        sarcinaService = new SarcinaService(sarcinaFileRepository);
        pontajService = new PontajService(pontajFileRepository,angajatFileRepository,sarcinaFileRepository);


        launch(args);
    }

    private void initView(Stage primaryStage) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/views/introduction.fxml"));
        AnchorPane layout = loader.load();
        primaryStage.setScene(new Scene((layout)));

        IntroductionController introductionController = loader.getController();
        introductionController.setAngajatiService(angajatService);
        introductionController.setSarciniService(sarcinaService);
        introductionController.setPontajeService(pontajService);
        introductionController.setIntroductionstage(primaryStage);
    }

}

