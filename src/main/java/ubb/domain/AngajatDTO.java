package ubb.domain;

import java.util.Objects;

public class AngajatDTO extends Entity <String> {
    private String nume;
    private Nivel nivel;
    private float SalarLuna;

    public AngajatDTO(String nume, Nivel nivel, float salarLuna) {
        this.nume = nume;
        this.nivel = nivel;
        SalarLuna = salarLuna;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public Nivel getNivel() {
        return nivel;
    }

    public void setNivel(Nivel nivel) {
        this.nivel = nivel;
    }

    public float getSalarLuna() {
        return SalarLuna;
    }

    public void setSalarLuna(int salarLuna) {
        SalarLuna = salarLuna;
    }

    @Override
    public String toString() {
        return "AngajatDTO{" +
                "nume='" + nume + '\'' +
                ", nivel=" + nivel +
                ", SalarLuna=" + SalarLuna +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AngajatDTO that = (AngajatDTO) o;
        return SalarLuna == that.SalarLuna &&
                Objects.equals(nume, that.nume) &&
                nivel == that.nivel;
    }

    @Override
    public int hashCode() {
        return Objects.hash(nume, nivel, SalarLuna);
    }
}
