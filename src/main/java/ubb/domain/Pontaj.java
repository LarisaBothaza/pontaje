package ubb.domain;


import java.time.LocalDateTime;
import java.util.Objects;

public class Pontaj extends Entity<Tuple<String,String>>{

    private LocalDateTime data;

    @Override
    public String toString() {
        return "Pontaj{" +
                super.getId().getLeft() +";"+
                super.getId().getRight() +";"+
                "data=" + data +
                '}';
    }

    public Pontaj(LocalDateTime data) {
        this.data = data;
    }

    public String getAngajat(){
        return super.getId().getLeft();
    }

    public String getSarcina(){
        return super.getId().getRight();
    }


    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }


}
